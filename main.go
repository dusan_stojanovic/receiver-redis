package main

import (
	"fmt"
	"log"
	"receiver-redis/redisclient"
)

func main() {
	log.Println("Initializing Redis Subscriber")
	subscriber := redisclient.InitRedisSubscriber()
	_, err := subscriber.ReceiveMessage()
	if err != nil {
		fmt.Println("Error: ", err)
	}
}
