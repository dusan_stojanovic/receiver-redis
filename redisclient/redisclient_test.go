package redisclient

import (
	"context"
	"encoding/json"
	"testing"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/stretchr/testify/assert"
)

func TestReceiveMessage(t *testing.T) {
	subscriber := initTest()

	go func() {
		time.Sleep(2 * time.Second)
		subscriber.redisSubscriber.Close()
	}()

	num, _ := subscriber.ReceiveMessage()
	assert.Equal(t, num, 1)
}

func initTest() *Subscriber {
	// Init subscriber
	redisClientSub := redis.NewClient(
		&redis.Options{
			Addr: "localhost:6379",
		},
	)
	ctx := context.Background()
	subscriber := redisClientSub.Subscribe(ctx, "test")

	// Init publisher
	redisClientPub := redis.NewClient(
		&redis.Options{
			Addr: "localhost:6379",
		},
	)

	// Send test message
	message := MessageDto{
		Content: "Test message",
	}
	body, _ := json.Marshal(message)

	redisClientPub.Publish(ctx, "test", body).Err()

	// Return subscriber
	return &Subscriber{
		redisSubscriber: subscriber,
	}
}
