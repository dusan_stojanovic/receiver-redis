package redisclient

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-redis/redis/v8"
)

type Subscriber struct {
	redisSubscriber *redis.PubSub
}

type MessageDto struct {
	Content string `json:"content,omitempty"`
}

func InitRedisSubscriber() *Subscriber {
	redisClient := redis.NewClient(
		&redis.Options{
			Addr: "localhost:6379",
		},
	)

	ctx := context.Background()
	sub := redisClient.Subscribe(ctx, "messages")

	go func() {
		// Listen to operating system's interrupt signal
		interrupt := make(chan os.Signal, 1)
		signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)
		<-interrupt

		// Gracefully shut down the client when it happens
		redisClient.Close()
		sub.Close()
	}()

	return &Subscriber{
		redisSubscriber: sub,
	}
}

func (s Subscriber) ReceiveMessage() (int, error) {
	ctx := context.Background()
	var message MessageDto
	num := 0

	for {
		redisMessage, err := s.redisSubscriber.ReceiveMessage(ctx)
		if err != nil {
			return num, err
		}

		err = json.Unmarshal([]byte(redisMessage.Payload), &message)
		if err != nil {
			return num, err
		}

		fmt.Println(message.Content)
		num++
	}
}
